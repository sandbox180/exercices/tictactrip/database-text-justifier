const {
  MONGO_INITDB_ROOT_USERNAME : rootUserName ,
  MONGO_INITDB_ROOT_PASSWORD : rootPassword,
  MONGO_API_DB : apiDatabase,
  MONGO_API_DB_USERNAME : apiUsername,
  MONGO_API_DB_PASSWORD : apiPassword,
  MONGO_TEST_DB : devDatabase,
} = process.env

const mainDB = db.getSiblingDB(apiDatabase);
const adminDB = db.getSiblingDB('admin');
adminDB.auth(rootUserName, rootPassword);
const apiRoles = [
    { role: "readWrite", db: apiDatabase },
    { role: "readWrite", db: devDatabase }
];
mainDB.createUser({
    user: apiUsername,
    pwd: apiPassword,
    roles: apiRoles
});
console.log("Yeeeeh ! Api user created !");
