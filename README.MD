#DOCKERFILE


**ENVIRONMENT VARIABLES**

```ini
MONGO_INITDB_ROOT_USERNAME=<string>
MONGO_INITDB_ROOT_PASSWORD=<string>
MONGO_API_DB=<string>
MONGO_API_DB_USERNAME=<string>
MONGO_API_DB_PASSWORD=<string>
```

**MANUAL BUILDING COMMAND**

Run this command
```bash
make build <tag (optional)>
```

Build container with
- `latest` 
- `<tag>` (optional)
tags and push it in container registry

**MAKEFILE CONFIG**
Edit container registry in `./makefile` file

```init
DOCKER_REGISTRY=<docker_registry_url>
```